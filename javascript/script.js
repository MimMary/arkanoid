const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

ctx.canvas.width = innerWidth * 0.4; 
ctx.canvas.height = innerHeight * 0.9;

let gameStarted = false;
let gameTimer = null;
let gameInterval = 10;

let brickHeight = 40;

let paddleWidth = 140;
let paddleHeight = 15;
let paddleDx = 8;

let ballRadius = 10;

let ball = {};
let paddle = {};
let bricks = [];
let colors = [
  "#59981A",
  "#ECF87F",
  "#81B622",
  "#3D550C",
  "#94C973",
  "#2F5233",
  "#B1D8B7",
  "#76B947",
  "#5CD85A",
  "#107869",
  "#1A5653",
];

createGame();

function createGame() {
  paddle = {
    h: paddleHeight,
    w: paddleWidth,
    y: canvas.height - 50,
    x: (canvas.width - paddleWidth) / 2,
    dx: paddleDx,
    rightPressed: false,
    leftPressed: false,
  };

  ball = {
    x: canvas.width / 2,
    y: paddle.y - ballRadius,
    radius: ballRadius,
    color: "#0E787C",
    dx: 2, 
    dy: -2,
  };

  for (let i = 0; i < getRandomNumber(3, 8); i++) {
    let randomNumber = getRandomNumber(3, 8);
    let width = Math.round(canvas.width / randomNumber);
    for (let j = 0; j < randomNumber; j++) {
      bricks.push({
        x: j * width,
        y: i * brickHeight,
        w: width,
        h: brickHeight,
        c: colors[Math.floor(Math.random() * colors.length)],
      });
    }
  }

  gameStarted = false;
  draw();
}

function startGame() {
  gameTimer = setInterval(draw, gameInterval);
}

function clear() {
  ctx.fillStyle = "#E9EAEC";
  ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

function drawBall() {
  ctx.fillStyle = ball.color;
  ctx.beginPath();
  ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI);
  ctx.fill();
  ctx.closePath();
}

function drawPaddle() {
  ctx.fillStyle = "#085252";
  ctx.beginPath();
  ctx.fillRect(paddle.x, paddle.y, paddle.w, paddle.h);
}

function drawBricks() {
  for (let i = 0; i < bricks.length; i++) {
    ctx.fillStyle = bricks[i].c;
    ctx.fillRect(bricks[i].x, bricks[i].y, bricks[i].w, bricks[i].h);
    ctx.strokeStyle = "#E9EAEC";
    ctx.strokeRect(bricks[i].x, bricks[i].y, bricks[i].w, bricks[i].h);
  }
}

function draw() {
  ball.x += ball.dx;
  ball.y += ball.dy;

  if (paddle.rightPressed && paddle.x <= canvas.width - paddle.w) {
    paddle.x += paddle.dx;
  } else if (paddle.leftPressed && paddle.x >= 0) {
    paddle.x -= paddle.dx;
  }

  clear();
  drawBall();
  drawPaddle();
  drawBricks();

  if (ball.x + ball.radius >= canvas.width || ball.x - ball.radius <= 0) {
    ball.dx = -ball.dx;
  }

  if (ball.y - ball.radius <= 0) {
    ball.dy = -ball.dy;
  }

  if (
    Math.floor(ball.x + ball.radius) >= paddle.x &&
    Math.floor(ball.x - ball.radius) <= paddle.x + paddle.w &&
    Math.floor(ball.y + ball.radius) >= paddle.y
  ) {
    ball.dy = -ball.dy;
  }

  for (let i = 0; i < bricks.length; i++) {
    if (
      Math.floor(ball.x + ball.radius) >= bricks[i].x &&
      Math.floor(ball.x - ball.radius) <= bricks[i].x + bricks[i].w &&
      Math.floor(ball.y + ball.radius) >= bricks[i].y &&
      Math.floor(ball.y - ball.radius) <= bricks[i].y + bricks[i].h
    ) {
      bricks.splice(i, 1);
      ball.dy = -ball.dy;
    }
  }

  if (ball.y >= canvas.height - ball.radius) {
    alert("The game is over");
    clearInterval(gameTimer);
    createGame();
  }

  if (!bricks.length) {
    alert("You're the winner!");
    clearInterval(gameTimer);
    createGame();
  }
}

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

document.addEventListener("keydown", function (e) {
  if (e.key == 39 || e.key == "ArrowRight") {
    paddle.rightPressed = true;
  } else if (e.key == 37 || e.key == "ArrowLeft") {
    paddle.leftPressed = true;
  }
});

document.addEventListener("keyup", function (e) {
  if (e.key == 39 || e.key == "ArrowRight") {
    paddle.rightPressed = false;
  } else if (e.key == 37 || e.key == "ArrowLeft") {
    paddle.leftPressed = false;
  }
});

document.addEventListener("keypress", function (e) {
  if (!gameStarted && (e.key == 32 || e.key == " ")) {
    gameStarted = true;
    startGame();
  }
});